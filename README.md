# Introduction

One of the main goals of this meetup is the classification of EEG signals by using Deep Learning methods.  We currently focused on using well known data sets but plan on using EEG hardware in the near future once adequate progress has been made.

## Getting Started

In order to get started with EEG classification, choose a data set that best fits your interest (we are shifting our focus to the latter two though):
* NeuroTechX Montreal, Pytorch, and Horizon BNCI Data
* MNE Visualization with the eegmmidb Data Set
* Kaggle "Grasp and Lift" Data Set and CSP+LDA

### Learning Python
If you need resources on learning Python please send an email to nycbci.meetup@gmail.com .
We will get you started in terms of learning Python and Deep Learning using Python.

### Deep Learning and EEG Classification with the BNCI Horizon Data Set

We initially started hoping to use only DL methods to classify EEG signals but decided to go with a hybrid approach of using both classical and DL methods.  This hybrid approach is also reflected in the NeuroTechX repo.

We are closely following the efforts of the NeuroTechX Montreal group which uses "braindecode" and MNE as the EEG specific modules for their work.  See https://github.com/robintibor/braindecode for braindcode and https://www.martinos.org/mne/stable/getting_started.html for MNE.  Braindecode was used for the paper "Deep learning with convolutional neural networks for EEG decoding and visualization" https://arxiv.org/abs/1703.05051

To get started with this data set and Google Colab, import our G-drive version of NeurotechX's code from here:
https://gitlab.com/nyc-bci/nyc-dl-eeg-playground/blob/g-drive/bci_two_classes_classification_colab.ipynb

To get started on your GPU workstation, download the data set "Two class motor imagery (002-2014)" from:
http://bnci-horizon-2020.eu/database/data-sets and use the Jupyter notebook as a script template.

### MNE Data Set

The easiest way to get started with this data set is to download the "Motor imagery decoding from EEG data using the Common Spatial Pattern (CSP)" example as a notebook or script.  The data set will automatically download during your first run. Here is the example code:
https://martinos.org/mne/stable/auto_examples/decoding/plot_decoding_csp_eeg.html

### Kaggle Data Set and CSP

Alexandre's, http://alexandre.barachant.org/ , competition winning code is less than 200 lines and uses CSP.  It's a good example of using MNE and classic methods before venturing off into Deep Learning approaches.  For now, you'll have to log into Kaggle for the data set but the code can be found here:
https://www.kaggle.com/alexandrebarachant/common-spatial-pattern-with-mne/code

## GANs and Synthetic EEG Data

Another component of the project will be examining how to produce artificial EEG signals that are based off of simple motor tasks. One of the papers guiding this effort looks at using Generative Adversarial Networks to produce motor task based EEG brainwaves. 

Here's the link to the paper.
https://arxiv.org/abs/1806.01875

Also here's a link about Wasserstein GANs.
https://arxiv.org/pdf/1701.07875.pdf

## DIY EEG devices

We don't yet have a related repo but we do have a wiki that outlines a possible approach here:
https://gitlab.com/nyc-bci/nyc-dl-eeg-playground/wikis/DIY-EEG-Devices

## Fun Lab: BCI with Commodity Headsets

This lab/project is focused on getting participants started with EEG and BCI by using simple commodity headsets.  The idea is to develop EEG headset applictions
to control animations or even a video game.  Find out more about this project on its wiki page:
https://gitlab.com/nyc-bci/start-here/wikis/Fun-Lab:-BCI-with-Commodity-Headsets

### 3D Printing Resources

[NYC Resistor](https://www.nycresistor.com/)

[Fat Cat Fab Lab](https://www.fatcatfablab.org/)

[Voodoo Manufacturing](https://voodoomfg.com/)